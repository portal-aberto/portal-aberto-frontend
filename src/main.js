import "./assets/reset.css";
import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import App from "./App.vue";
import router from "./router";
import messages from "./i18n/pt-br";
import installElementPlus from "./plugins/element";
import { store } from "./modules/user-module";
import axios from "axios";
import mitt from "mitt";

axios.defaults.withCredentials = true;
axios.defaults.headers.post["Content-Type"] = "application/json;charset=UTF-8";
axios.defaults.baseURL = "http://psicologia.ip.usp.br:4100";

const i18n = createI18n({
  locale: "ptBr",
  messages: messages,
});

const emitter = mitt();
const app = createApp(App);
app.config.globalProperties.emitter = emitter;

installElementPlus(app);
app.use(store);
app.use(i18n);

app.use(router).mount("#app");

app.config.errorHandler = function (err, vm) {
  if (
    (err.response && err.response.status === 401) ||
    err.response.status === 403
  ) {
    vm.$alert(vm.$t("login.expired"), "Atenção", {
      confirmButtonText: "OK",
    });
    return Promise.reject(err.response);
  }
  if (err.response && err.response.status > 400 && err.response.status < 500) {
    if (Array.isArray(err.response.data.message)) {
      vm.$alert(
        err.response.data.message.map((key) => vm.$t(key)).join(" | "),
        "Atenção",
        {
          confirmButtonText: "OK",
        }
      );
    } else {
      vm.$alert(vm.$t(err.response.data.message), "Atenção", {
        confirmButtonTexts: "OK",
      });
    }
    return Promise.reject(err.response);
  }
  vm.$alert(vm.$t("general.error"), "Atenção", {
    confirmButtonText: "OK",
  });
  return Promise.reject(err.response);
};

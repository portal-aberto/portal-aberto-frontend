import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";

export const store = createStore({
  state() {
    return {
      loggedInStatus: "NOT_LOGGED_IN",
      user: {},
    };
  },
  mutations: {
    userStatus(state, payload) {
      state.loggedInStatus = payload.loggedInfo;
      state.user = JSON.parse(JSON.stringify(payload.userData));
    },
  },
  plugins: [createPersistedState()],
});

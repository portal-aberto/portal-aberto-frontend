import { createWebHistory, createRouter } from "vue-router";
import Home from "../components/home/Home.vue";
import About from "../components/about/About.vue";
import CreateItem from "../components/admin/books/CreateItem.vue";
import BookSearch from "../components/admin/books/Search.vue";
import BooksAdminDetails from "../components/admin/books/Details.vue";
import UserAdminSearch from "../components/admin/users/Search.vue";
import UserAdminDetails from "../components/admin/users/Details.vue";
import TagAdminSearch from "../components/admin/tags/Search.vue";
import TagAdminCreate from "../components/admin/tags/CreateItem.vue";
import TagAdminDetail from "../components/admin/tags/Details.vue";
import Login from "../components/login/Login.vue";
import Register from "../components/register/Register.vue";
import EmailConfirmation from "../components/register/EmailConfirmation.vue";
import Profile from "../components/user/Profile.vue";
import Comments from "../components/user/Comments.vue";
import Settings from "../components/user/Settings.vue";
import BooksDetails from "../components/books/Details.vue";
import ResetPassword from "../components/user/settings/ResetPassword.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/admin/books",
    name: "BookSearch",
    component: BookSearch,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/register/confirm_email/:token",
    name: "EmailConfirmation",
    component: EmailConfirmation,
  },
  {
    path: "/user/profile",
    name: "Profile",
    component: Profile,
    children: [
      {
        path: "/user/comments",
        name: "Comments",
        component: Comments,
      },
      {
        path: "/user/settings",
        name: "Settings",
        component: Settings,
      },
    ],
  },
  {
    path: "/user/reset_password/:token",
    name: "ResetPassword",
    component: ResetPassword,
  },
  {
    path: "/books/:id",
    name: "BooksDetails",
    component: BooksDetails,
  },
  /* admin */
  {
    path: "/admin/create",
    name: "create",
    component: CreateItem,
  },
  {
    path: "/admin/books/:id",
    name: "BooksAdminDetails",
    component: BooksAdminDetails,
  },
  {
    path: "/admin/users/:id",
    name: "UserAdminDetails",
    component: UserAdminDetails,
  },
  {
    path: "/admin/users",
    name: "UserAdminSearch",
    component: UserAdminSearch,
  },
  {
    path: "/admin/tags",
    name: "TagAdminSearch",
    component: TagAdminSearch,
  },
  {
    path: "/admin/tags/create",
    name: TagAdminCreate,
    component: TagAdminCreate,
  },
  {
    path: "/admin/tags/:id",
    name: TagAdminDetail,
    component: TagAdminDetail,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;

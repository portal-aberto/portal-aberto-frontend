import "./assets/reset.css";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import installElementPlus from "./plugins/element";
import { store } from "./modules/user-module";
const app = createApp(App);
installElementPlus(app);
app.use(store);
app.use(router).mount("#app");

export default app;

import axios from "axios";
export const registerUser = function (requestBody) {
  return axios.post(`/registrations`, { user: requestBody });
};

export const confirmEmail = function (requestBody) {
  return axios.post(`/registrations/confirm_email`, { token: requestBody });
};

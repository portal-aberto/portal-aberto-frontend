import { stringify } from "querystring";
import axios from "axios";

export const getBookComments = function (id, params) {
  var urlParams = stringify(params);
  return axios.get(`/books/${id}/comments?${urlParams}`);
};

export const getUserComments = function (id, params) {
  var urlParams = stringify(params);
  return axios.get(`/users/${id}/comments?${urlParams}`);
};

export const createComment = function (id, requestBody) {
  return axios.post(`/books/${id}/comments`, requestBody);
};

export const deleteComment = function (id, requestBody) {
  return axios.delete(
    `/books/${id}/comments/${requestBody.comment_id}`,
    requestBody
  );
};

import { stringify } from "querystring";
import axios from "axios";

export const searchBooks = function (params) {
  var urlParams = stringify(params, "&", "=", "oi");
  return axios.get(`/books?${urlParams}`);
};

export const createBook = function (requestBody) {
  return axios.post("/books", requestBody);
};

export const deleteBook = function (id) {
  return axios.delete(`/books/${id}`);
};

export const getBook = function (id) {
  return axios.get(`/books/${id}`);
};

export const updateBook = function (id, requestBody) {
  return axios.put(`/books/${id}`, requestBody);
};

export const createBookCSV = function (requestBody) {
  const data = new FormData();
  data.append("csv_file", requestBody, { type: "text/csv" });
  return axios.post("/books/csv", data, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

export const rateBook = function (id, requestBody) {
  return axios.patch(`/books/${id}/ratings`, requestBody);
};

import axios from "axios";
export const createSession = function (requestBody) {
  return axios.post(`/sessions`, { user: requestBody });
};

export const getCurrentUser = function () {
  return axios.get("/logged_in");
};

export const logOut = function () {
  return axios.delete("/logout");
};

import axios from "axios";
export const googleApiEndpoint = "https://www.googleapis.com/books/v1/volumes";

export const searchGoogleBooksByIsbn = function (isbn) {
  return axios.get(`${googleApiEndpoint}?q=isbn:${isbn}`, {
    withCredentials: false,
  });
};

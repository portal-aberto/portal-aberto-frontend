import { stringify } from "querystring";
import axios from "axios";

export const getBookTags = function (id) {
  return axios.get(`/books/${id}/tags`);
};

export const searchTags = function (params) {
  var urlParams = stringify(params);
  return axios.get(`/tags?${urlParams}`);
};

export const getTag = function (id) {
  return axios.get(`/tags/${id}`);
};

export const createTag = function (requestBody) {
  return axios.post(`/tags`, requestBody);
};

export const updateTag = function (id, requestBody) {
  return axios.put(`/tags/${id}`, requestBody);
};

export const deleteTag = function (id) {
  return axios.delete(`/tags/${id}`);
};

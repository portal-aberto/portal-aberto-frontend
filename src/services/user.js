import { stringify } from "querystring";
import axios from "axios";

export const searchUsers = function (params) {
  var urlParams = stringify(params);
  return axios.get(`/users?${urlParams}`);
};
export const getUser = function (id) {
  return axios.get(`/users/${id}`);
};

export const updateUserType = function (id, requestBody) {
  return axios.put(`/users/${id}`, requestBody);
};

export const deleteUser = function (id) {
  return axios.delete(`/users/${id}`);
};

export const updateProfile = function (requestBody) {
  return axios.post("/update_profile", { user: requestBody });
};

export const deleteOwnUser = function () {
  return axios.delete("/delete_own_user");
};

export const updatePwd = function (requestBody) {
  return axios.patch("/update_password", { user: requestBody });
};

export const recoverPwd = function (requestBody) {
  return axios.patch("/recover_password", { user: requestBody });
};

export const resetPwdLinkStatus = function (requestBody) {
  return axios.post("/reset_password_link_status", { token: requestBody });
};

export const resetPwd = function (requestBody) {
  return axios.patch("/reset_password", { user: requestBody });
};

// import { expired } from './errorRescuer';

import { expired } from "./errorRescuer";

export const errorResp = (error) => {
  if (
    (error.response && error.response.status === 401) ||
    error.response.status === 403
  ) {
    expired();
    return Promise.reject(error.response);
  }
  console.info("não deu certo!");
  console.info(error.response.status);
  return Promise.reject(error);
};

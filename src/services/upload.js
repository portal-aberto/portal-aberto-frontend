import axios from "axios";

export const deleteImage = function (url) {
  return axios.delete(url);
};

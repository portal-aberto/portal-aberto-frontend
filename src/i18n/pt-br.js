export default {
  ptBr: {
    general: {
      error: "Aconteceu algum problema.",
    },
    login: {
      expired: "Sua sessão expirou!",
    },
    "Code can't be blank": "Código não pode estar vazio",
    "Code is too short (minimum is 2 characters)":
      "Código está muito curto (o mínimo é de 2 caracteres)",
    "Code is too long (maximum is 20 characters)":
      "Código está muito longo (o máximo é de 20 caracteres)",
    "Value can't be blank": "Valor não pode ser vazio",
    "Value is too short (minimum is 2 characters)":
      "Valor está muito curto (o mínimo é de 2 caracteres)",
    "Value is too long (maximum is 100 characters)":
      "Valor está muito longo (o máximo é de 100 caracteres)",
    "Code is already taken": "Código já existe",
    "Password can't be blank": "Senha não pode ser vazia",
    "Username can't be blank": "Username não pode ser vazio",
    "Username is invalid": "Username pode ter apenas letras ou números",
    "Password is too short (minimum is 8 characters)":
      "Senha muito curta (mínimo de 8 caracteres)",
    "Email can't be blank": "Email não pode ser vazio",
    "Username is already taken": "Username já existe",
    "User not found": "Usuário não encontrado",
    "Book not found": "Livro não encontrado",
    "Tag not found": "Tag não encontrada",
    "Email is already taken": "Email já utilizado",
  },
};

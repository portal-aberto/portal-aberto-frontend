# Portal Aberto Frontend

Aplicação Front-end do Portal Aberto do [Instituto de Psicologia da USP](https://www.usp.br/interpsi/)

Esse projeto utiliza vue 3 e npm.

Para informações sobre como executar o deploy da aplicação, acessar essa [documentação](https://gitlab.com/portal-aberto/portal-aberto-deploy/-/blob/master/README.md).

# Choose the Image which has Node installed already
FROM ebiven/vue-cli

RUN mkdir -p /app
WORKDIR /app

# # install simple http server for serving static content
# RUN npm install -g http-server

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

# install project dependencies
RUN npm install -g serve
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build

EXPOSE 80
# CMD [ "npm", "run", "serve" ]
CMD [ "serve", "-s", "dist", "-l", "80" ]
